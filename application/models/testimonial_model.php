<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Testimonial_model extends CI_Model
{
	public function __construct() 
	{ 
		parent::__construct(); 
	}

	//fungsi yang mengembalikan data-data testimonial yang aktif sebanyak 4 tuple
	public function get_testimonial(){
		//example : $this->db->get_where('mytable', array('id' => $id), $limit, $offset);
		$query = $this->db->get_where("sys_ms_testimonial",array("testimonial_status"=>1),4);  
		return $query->result();
	}

	public function add_testimonial($data)
	{
		$this->db->insert('sys_ms_testimonial', $data);
	}

}
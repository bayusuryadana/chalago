<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Email_model extends CI_Model 
{
	public function __construct() 
	{ 
		parent::__construct(); 
	}

	/**
	 * fungsi yang digunakan untuk mengirim email
	 * @send_to tujuan
	 * @message_subject judul email
	 * @message_content isi email
	*/
	//$send_to, $message_subject, $message_content
	public function send_email()
	{
		//create template
		//send with aws?
	}

	public function acc_verification($user_id, $user_email)
	{
		$activation_code = $this->user_model->get_activation_code($user_id);
		//create content email and subject
		$this->send_email($user_email, $message_subject, $message_content);
	}

	public function reminder_ch_5($user_email)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}

	public function reminder_ch_1($user_email)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}

	public function post_submit($user_email)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}

	public function challenge_result($user_email)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}

	public function new_challenge($user_email, $data)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}

	// @data data-data yang berubah pada challenge
	public function update_challenge($user_email, $data)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}

	//@data data-data yang masuk ke dalam newsletter
	public function newsletter($user_email, $data)
	{
		//content
		$this->send_email($user_email, $message_subject, $message_content);
	}
	
}
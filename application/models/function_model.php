<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Function_model extends CI_Model
{
	public function __construct() 
	{ 
		parent::__construct(); 
	}

	/**
		* menghitung sisa hari dari waktu sekarang
		* @deadline timestamp yang ingin diketauhi berapa jaraknya dari saat ini
	*/
	public function get_time_left($deadline)
	{
        date_default_timezone_set('Asia/Jakarta');
		$now = date("Y-m-d H:i:s");
		$deadline_str = strtotime($deadline); //deadline in seconds
		$now_str = strtotime($now); //nowdays in seconds

		$diff = $deadline_str - $now_str;
	
		$hour   = 3600;
		$day    = 24*$hour;

		$ans["day"]    = floor(($diff)/$day);
		$ans["hour"]   = floor(($diff%$day)/$hour);

		if ($ans["day"] > 0){
			return $ans["day"] . " DAY(S) LEFT";
		} else 
		if ($ans["hour"] > 0){
			return $ans["hour"] . " HOUR(S) LEFT";
		} else {
			return "LESS THAN A HOUR LEFT";
		}
	}
}
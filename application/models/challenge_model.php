<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Challenge_model extends CI_Model
{
	public function __construct() 
	{ 
		parent::__construct(); 
	}
	/** fungsi yang mengembalikan data-data challenge yang dibutuhkan pada halaman utama
	 * @limit limit data yang diambil, null untuk ambil semua
	 * @get null untuk ambil semua, finished untuk ambil yang sudah berlalu, ongoing untuk ambil yang masih berjalan
	 */
	public function get_all_challenge($get=null,$limit=null)
	{
		date_default_timezone_set('Asia/Jakarta');
		$date_now = date("Y-m-d H:i:s");

		/*
		$this->db->select("ch_ms_challenge.*, brand_name, 
			GROUP_CONCAT(DISTINCT creative_field_name_en ORDER BY creative_field_name_en SEPARATOR ', ') as creative_field_name_en,
			SUM(ch_tr_prize.prize_money) as total_prize,
			COUNT(ch_tr_prize.prize_item) as special_prize
			", false);
		$this->db->from("ch_ms_challenge");

		//join to brand
		$this->db->join("ch_ms_brand","ch_ms_brand.brand_id = ch_ms_challenge.brand_id","left");

		//m-n relation challenge to creative field
		$this->db->join("ch_tr_category","ch_tr_category.challenge_id = ch_ms_challenge.challenge_id", "left");
		$this->db->join("sys_ms_creative_field","sys_ms_creative_field.creative_field_id = ch_tr_category.creative_field_id","left");

		$this->db->group_by("challenge_id");

		//join to prize
		$this->db->join("ch_tr_prize","ch_ms_challenge.challenge_id = ch_tr_prize.challenge_id","left");

		if ($get == "ongoing"){
			$this->db->where("challenge_deadline >",$date_now);
		} else
		if ($get == "finished"){
			$this->db->where("challenge_deadline <",$date_now);
		}
		if ($limit != null){
			$this->db->limit($limit);
		}
		$this->db->order_by("challenge_deadline","desc");
		*/

		$this->db->select("tmp.*, brand_name, 
			SUM(ch_tr_prize.prize_money) as total_prize, 
    		COUNT(ch_tr_prize.prize_item) as special_prize 
			");

		$this->db->from(" (
			SELECT ch_ms_challenge.* , 
			GROUP_CONCAT(DISTINCT creative_field_name_en ORDER BY creative_field_name_en SEPARATOR ', ') AS creative_field_name_en
			FROM ch_ms_challenge
			LEFT JOIN ch_tr_category ON ch_tr_category.challenge_id = ch_ms_challenge.challenge_id
			LEFT JOIN sys_ms_creative_field ON sys_ms_creative_field.creative_field_id = ch_tr_category.creative_field_id
			GROUP BY ch_ms_challenge.challenge_id
		    ) tmp
			", false);

		$query = $this->db->get();

		return $query->result();
	}

	public function get_challenge($challenge_id)
	{
		$this->db->select("ch_ms_challenge.*, brand_name, 
			GROUP_CONCAT(DISTINCT creative_field_name_en ORDER BY creative_field_name_en SEPARATOR ', ') as creative_field_name_en, 
			SUM(ch_tr_prize.prize_money) as total_prize, 
			COUNT(ch_tr_prize.prize_item) as special_prize
			", false);
		$this->db->from("ch_ms_challenge");

		//join to brand
		$this->db->join("ch_ms_brand","ch_ms_brand.brand_id = ch_ms_challenge.brand_id","left");

		//m-n relation challenge to creative field
		$this->db->join("ch_tr_category","ch_tr_category.challenge_id = ch_ms_challenge.challenge_id", "left");
		$this->db->join("sys_ms_creative_field","sys_ms_creative_field.creative_field_id = ch_tr_category.creative_field_id","left");

		//join to prize
		$this->db->join("ch_tr_prize","ch_ms_challenge.challenge_id = ch_tr_prize.challenge_id","left");

		$this->db->where("ch_ms_challenge.challenge_id",$challenge_id);
		$query = $this->db->get();

		return $query->row();
	}

}
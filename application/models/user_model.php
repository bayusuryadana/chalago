<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
	/**
	 * CONST
	 * 0 = Unverified User
	 * 1 = Banned User
	 */
	const SPECIAL_STAT_NONE = 200;
	const SPECIAL_STAT_UNVERIFIED = 0;
	const SPECIAL_STAT_BANNED = 1;

	public function __construct() 
	{ 
		parent::__construct(); 
	}
	
	//insert data
	public function register_user($data,$specialities = array())
	{
		$data['user_password'] = $this->get_hashed_password($data['user_password']);
		$this->db->insert('us_ms_user', $data);
		$user = $this->get_users($data)[0];
		$this->add_speciality($user->user_id,$specialities);
		$this->add_special_stat($user->user_id,self::SPECIAL_STAT_UNVERIFIED);
	}

	public function add_speciality($user_id,$specialities)
	{	
		$this->db->delete('us_tr_speciality', array('user_id' => $user_id)); 	
		$data = array();
		foreach($specialities as $sp)
		{
			$tmp['user_id'] = $user_id;
			$tmp['creative_field_id'] = $sp;
			$tmp['speciality_order'] = count($data) + 1;
			$data[] = $tmp;
		}
		$this->db->insert_batch('us_tr_speciality', $data); 
	}

	public function add_special_stat($user_id,$special_stat_code)
	{	
		$this->db->delete('us_tr_special_stat', array('user_id' => $user_id)); 	
		$data['user_id'] = $user_id;
		$data['special_stat_code'] = $special_stat_code;
		$this->db->insert('us_tr_special_stat', $data); 
	}	

	//read data
	public function get_user($atribute)
	{ 
		$query = $this->get_users($atribute);
		if($query !== false){
			return $query[0];
		}else{
			return false;
		}
	}

	public function get_password($atribute)
	{
		$this->db->select('user_id, user_email, user_password');
		$this->db->from('us_ms_user');
		$this->db->where($atribute); 
		$query = $this->db->get();
		if($query->num_rows > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function get_users($atribute)
	{
		$this->db->select('*');
		$this->db->from('us_ms_user');
		$this->db->where($atribute); 
		$query = $this->db->get();
		if($query->num_rows > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function get_user_speciality($user_id)
	{
		$this->db->select('speciality_order, creative_field_name_id,creative_field_name_en');
		$this->db->from('us_tr_speciality');
		$this->db->join('sys_ms_creative_field', 'us_tr_speciality.creative_field_id = sys_ms_creative_field.creative_field_id');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		if($query->num_rows > 0){
			return $query->result();
		}else{
			return false;
		}	
	}

	public function update_user($user_id,$data)
	{
		$this->db->where('user_id', $id);
		$this->db->update('us_ms_user', $data); 
	}

	public function update_password($user_id,$password)
	{
		$data['user_password'] = $this->get_hashed_password($password); 
		$this->db->where('user_id', $id);
		$this->db->update('us_ms_user', $data); 
	}
	//generate-an
	public function get_hashed_password($str)
	{
		return md5($str);
	}

	public function get_activation_code($user_id)
	{
		$user = $this->get_user(array('user_id'=>$user_id));
		return md5($user->user_id.$user->user_email);
	}

	public function get_user_special_stat($user_id)
	{
		$this->db->select("*");
		$this->db->from('us_tr_special_stat');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		$ret = "";
		if($query->num_rows > 0){
			$stat = $query->result()[0]->special_stat_code;
			$ret['code'] = $stat;
			$ret['description'] = $this->get_special_stat_description($stat);
		}else{
			$ret['code'] = self::SPECIAL_STAT_NONE;
			$ret['description'] = $this->get_special_stat_description(self::SPECIAL_STAT_NONE);
		}	

		return $ret;
	}

	public function get_special_stat_description($special_stat_code)
	{
		if($special_stat_code == self::SPECIAL_STAT_NONE){
			return "Tidak ada masalah";
		}
		else if($special_stat_code == self::SPECIAL_STAT_UNVERIFIED){
			return "Belum verifikasi";
		}else if($special_stat_code == self::SPECIAL_STAT_BANNED)
		{
			return "Banned";
		}else{
			return "Tidak tidak jelas :|";
		}
	}


	public function activate_user($user_id, $activation_code)
	{
		if($this->get_user_special_stat($user_id)['code'] ==  self::SPECIAL_STAT_UNVERIFIED){
			if($activation_code === $this->get_activation_code($user_id))
			{
				$this->remove_special_stat($user_id,self::SPECIAL_STAT_UNVERIFIED);
				return true;
			}
		}

		return false;
	}

	public function remove_special_stat($user_id,$special_stat_code)
	{
		$this->db->delete('us_tr_special_stat', array('user_id' => $user_id,'special_stat_code'=>$special_stat_code)); 	
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class System_model extends CI_Model
{
	public function __construct() 
	{ 
		parent::__construct(); 
	}

	public function get_creative_field(){
		$this->db->select("*"); 
		$this->db->order_by("creative_field_id", "asc"); 
		$query = $this->db->get('sys_ms_creative_field');
		return $query->result();
	}
}


    <div class="showcase">
      <h1>ACCEPT <br>CREATIVE CHALLENGES <br>FROM BRANDS</h1>
      <h4>AND WIN PRIZES <span><img src="<?=base_url()?>media/image/icon/tandaseru.png"></span></h4>
      <button href="#" id="button-chalago-xl">LEARN MORE <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button>
    </div>

    <!-- start on going challenge-->
    <div class="on-going-challenge">
      <div class="container">
        <h1>CHALLENGES</h1>

        
        <div class="row">

          <?php
          foreach ($challenges as $row) {
          ?>
          <div class="col-md-6 hitam">  
          <div class="container-thumbnail">
          <div class="black">   
            <div class="description-thumbnail">
              <h2><?=strtoupper($row->challenge_title)?></h2>
              <p id="desc"><?=$row->brand_name?></p>
              <p><?=$row->creative_field_name_en?></p>
              <p><img id="cup" src="<?=base_url()?>media/image/icon/Icon-cup.png">
                <span id="prize"><?="Rp. ".number_format($row->total_prize)?></span></p>
              <p id="deadline"><?=$row->challenge_deadline?></p>
              <a href="<?=base_url()?>challenge/view/<?=space_to_dash($row->challenge_title)?>/<?=$row->challenge_id?>" id="button-chalago-s">JOIN NOW <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></a>
            </div>
            </div>
            </div>    
          </div>
          <?php
          }
          ?>

        </div><!-- end row item challenges-->
        
        <h4><a href="<?=base_url()?>challenge">VIEW MORE CHALLENGES</a></h4>
      </div><!-- end of container-->
    </div><!-- end on going challenge-->

    <!-- start on testimonial-->
    <div class="testimonial">
      <div class="container">
        <h1>WHAT THEY SAY ABOUT US</h1>
        <div class="row">
          <?php
          foreach ($testimonials as $row) {
          ?>
          <div class="col-xs-12 col-sm-12 col-md-6 paket-testi">
            <div class="row">
              <div class=" col-xs-3 col-md-3"> 
              <img class="image-ava" src="<?=base_url()?>data/Ava01.png">
              </div>
              <div class="testi col-xs-offset-1 col-md-offset-1 col-xs-7 col-md-7">
                <p><?=$row->testimonial_content?></p>
                <br>
                <p id="name"><?=$row->testimonial_name?></p>
                <p><?=$row->testimonial_profession?></p>
              </div>
          </div>
        </div>

        <?php } ?>   

        </div>
      </div>
    </div>
    <div class="brands-get-started">
      BRANDS? LETS LAUNCH A CREATIVE CHALLENGE! <button href="#" id="button-chalago">GET STARTED <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button>
    </div>
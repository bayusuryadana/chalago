

    <div class="container-filter">
      <div class="container">
        <h2>CHALLENGES</h2>
      </div>
    </div>

    <div class="container-challenges">
      <div class="container">
        <div class="challenge-on-going">
          <h1>ON GOING CHALLENGES</h1>
          <div class="row">

            <?php
            foreach ($challenge_ongoing as $row) {
            ?>
            <div class="col-md-6 hitam">  
              <div class="container-thumbnail">
                <div class="black">   
                  <div class="description-thumbnail">
                    <h2><?=strtoupper($row->challenge_title)?></h2>
                    <p id="desc"><?=$row->brand_name?></p>
                    <p><?=$row->creative_field_name_en?></p>
                    <p><img id="cup" src="<?=base_url()?>media/image/icon/Icon-cup.png">
                    <span id="prize"><?="Rp. ".number_format($row->total_prize_money)?></span></p>
                    <p id="deadline"><?=$row->challenge_deadline?></p>
                    <a href="<?=base_url()?>challenge/view/<?=space_to_dash($row->challenge_title)?>/<?=$row->challenge_id?>" id="button-chalago-s">JOIN NOW <span><img class="arrow" src="media/image/icon/arrow.png"></span></a>
                  </div>
                </div>
              </div>    
            </div>
            <?php } ?>

          </div><!-- end row ongoing -->
        </div>

        <div class="challenge-finished">
          <h1>FINISHED CHALLENGES</h1>
          <div class="row">

            <?php
            foreach ($challenge_finished as $row) {
            ?>
            <div class="col-md-6 hitam">  
              <div class="container-thumbnail-finished">
                <div class="black">   
                  <div class="description-thumbnail-finished">
                    <h2><?=strtoupper($row->challenge_title)?></h2>
                    <p id="desc"><?=$row->brand_name?></p>
                    <p><?=$row->creative_field_name_en?></p>
                    <p><img id="cup" src="<?=base_url()?>media/image/icon/Icon-cup.png">
                    <span id="prize"><?="Rp. ".number_format($row->total_prize_money)?></span></p>
                    <p id="deadline">FINISHED</p>
                    <a href="<?=base_url()?>challenge/view/<?=space_to_dash($row->challenge_title)?>/<?=$row->challenge_id?>" id="button-chalago-s">VIEW RESULT <span><img class="arrow" src="media/image/icon/arrow.png"></span></a>
                  </div>
                </div>
              </div>    
            </div>
            <?php } ?>

          </div><!-- end row finished -->
        </div>



      </div>
    </div>

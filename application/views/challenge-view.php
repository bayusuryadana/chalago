
    <div class="header-challenge">

      <div class="menu-fix-when-scroll">
        <div class="container">
          <div class="row">
            <div class="col-xs-7 col-md-7">
              <p><?=$challenge->creative_field_name_en?></p>
              <h1><?=$challenge->challenge_title?></h1>
              <p><?=$challenge->brand_name?></p>
            </div>
            <div class="col-xs-5 col-md-offset-2 col-md-3">
              <button href="#" id="button-chalago-xl">JOIN <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button>
            </div>
          </div>
        </div>
      </div>

    </div>


    <div class="timeline">
       <div class="container">
            <div class="timeline-wrap">  
                    <div class="timeline-list-events">
                        <div class="event" style="left: 0%;">
                          <span class="event-title event-done">START</span>
                          <div class="timeline-dateblock-active"></div>
                          <span class="event-time">16 June 2014</span>
                        </div>
                        <div class="event" style="left: 25%;">
                          <span class="event-submission event-done">SUBMISSION CLOSED</span>
                          <div class="timeline-dateblock-active"></div>
                          <span class="event-time">16 July 2014</br>23.59</span>
                        </div>
                        <div class="event" style="left: 60%;">
                          <span class="event-title">JUDGING</span>
                          <div class="timeline-dateblock"></div>
                          <span class="event-time">17 July 2014</span>
                        </div>
                        <div class="event" style="left: 100%;">
                          <span class="event-title">RESULT</span>
                          <div class="timeline-dateblock"></div>
                          <span class="event-time">24 July 2014</span>
                        </div>
                    </div>  

                    <div class="timeline-bottom" style="left: 0px;">     
                        <div class="timeline-event-white"></div>      
                        <div class="timeline-event-red"></div>      
                    </div>
            </div>        
        </div>
    </div>   

    <div class="main-content">
      <div class="container">
        <div class="row">
          <div class=" col-xs-12 col-md-9">
            <div class="content-container" id="summary">
            <h2>SUMMARY</h2>
            <p><?=$challenge->challenge_brief?></p>
            </div>
            
            <div class="content-container" id="rules">
               
              <h2>RULES <span><a data-toggle="collapse" data-parent="#rules" href="#content-rules">
                        Show rules >></a></span>
              </h2>
              <div id="content-rules" class="panel-collapse collapse">
                <?=$challenge->challenge_rules?>
              </div>
              <form><input type="checkbox" required> I have read and accept the rules of this challenge</input>
                <div class="content-container"><button href="#" id="button-chalago-xl">JOIN <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button>
                </div>
              </form>
              <h5>Join the challenge to gain full access<br>to the brief and stay updated.</h5>
                
            </div>


          </div>
          <div class="col-xs-12 col-md-3">

            <!-- Responsive 768 - 992 
            <div class="row">
              <div class="subcontent-prize" id="total-prize-992">
                <h3>TOTAL PRIZE</h3>
                <p>Rp. 50.000.000,00</p>
              </div>
              <div class="subcontent-deadline" id="deadline-992">
                <h3>DEADLINE</h3>
                <p>16 JULY 2014</p>
                <p id="time">23.59 WIB</p>
              </div>
            </div>
            <!-- End of responsive -->
            

            <div class="subcontent-prize" id="total-prize">
              <h3>TOTAL PRIZE</h3>
              <p>Rp. 50.000.000,00</p>
            </div>
            <div class="subcontent-deadline" id="deadline">
              <h3>DEADLINE</h3>
              <p>16 JULY 2014</p>
              <p id="time">23.59 WIB</p>
            </div>
            <div class="subcontent-jury">
              <h3>JURY</h3>
              <div class="row">
                <div class="col-xs-4"><img class="image" src="<?=base_url()?>media/image/challenges/img-jury01.png"></div>
                <div class="col-xs-8">
                  <p class="jury-title">JURY NAME 01</p>
                  <p class="jury-desc">Art Director</p>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-4"><img class="image" src="<?=base_url()?>media/image/challenges/img-jury02.png"></div>
                <div class="col-xs-8">
                  <p class="jury-title">JURY NAME 02</p>
                  <p class="jury-desc">Art Director</p>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4"><img class="image" src="<?=base_url()?>media/image/challenges/img-jury03.png"></div>
                <div class="col-xs-8">
                  <p class="jury-title">CHALLAGO!</p>
                  <p class="jury-desc">Art Director</p>
                </div>
              </div>
              
            </div>

            <div class="subcontent-jury-992">
              <h3>JURY</h3>
              <div class="row">
              <div class="col-xs-4">
                <img class="image" src="<?=base_url()?>media/image/challenges/img-jury01.png">
                <p class="jury-title">JURY NAME 01</p>
                <p class="jury-desc">Description</p>
              </div>
              <div class="col-xs-4">
                <img class="image" src="<?=base_url()?>media/image/challenges/img-jury02.png">
                <p class="jury-title">JURY NAME 01</p>
                <p class="jury-desc">Description</p>
              </div>
              <div class="col-xs-4">
                <img class="image" src="<?=base_url()?>media/image/challenges/img-jury03.png">
                <p class="jury-title">JURY NAME 01</p>
                <p class="jury-desc">Description</p>
              </div>

              
              </div>
            </div>

            



            <div class="subcontent-submission">
              <h3>SUBMISSION</h3>
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
              <img src="<?=base_url()?>media/image/Tes.png">
            </div>

           <div class="content-container" id="rules-992">
               
              <h2>RULES <span><a data-toggle="collapse" data-parent="#rules-992" href="#content-rules-992">
                        Show rules >></a></span>
              </h2>
              <div id="content-rules-992" class="panel-collapse collapse">
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat.</p>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              
              </div>

                <form><input type="checkbox" required> I have read and accept the rules of this challenge</input>
                <div class="content-container"><button href="#" id="button-chalago-xl">JOIN <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button>
                </div>
              </form>
              <h5>Join the challenge to gain full access<br>to the brief and stay updated.</h5>
              
            </div>


          </div>
        </div>
      </div>
    </div> 
  
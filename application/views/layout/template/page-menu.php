    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span><img src="<?=base_url()?>media/image/general/Home-Menu.png"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url()?>"><img class="logo" src="<?=base_url()?>media/image/general/Chalago - Logo.png"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?=base_url()?>challenge">DISCOVER</a></li>
            <li><a href="#">LAUNCH</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=base_url('site/signup')?>">SIGN UP</a></li>
            <li><a href="<?=base_url('site/login')?>">LOGIN</a></li>
          </ul>
        </div>
      </div>
    </div>
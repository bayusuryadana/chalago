    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url('media/script/')?>/jquery.js"></script>
    <script src="<?=base_url('media/script/')?>/paralax-js/jquery.parallax-1.1.3.js"></script>
    <script src="<?=base_url('media/script/')?>/paralax-js/jquery.localscroll-1.2.7-min.js"></script>
    <script src="<?=base_url('media/script/')?>/paralax-js/jquery.scrollTo-1.4.2-min.js"></script>
    <script src="<?=base_url('media/script/')?>/bootstrap.min.js"></script>
    <script src="<?=base_url('media/script/')?>/smooth-scroll.js"></script>
    
    <?php if(isset($scripts) && count($scripts) > 0) { ?>
      <?php foreach($scripts as $script) { ?>
      <script src="<?=base_url('media/script/')."/".$script?>"></script>
      <?php } ?>
    <?php } ?>
  </body>
</html>

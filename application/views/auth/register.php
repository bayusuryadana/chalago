<html>
<head>
<title>My Form</title>
</head>
<body>

<?php echo validation_errors(); ?>

<?php echo form_open('site/register/'); ?>

<h5>Email Address</h5>
<?php echo form_error('user[user_email]'); ?>
<input type="text" name="user[user_email]" value="<?php echo set_value('user[user_email]',$user_email); ?>" size="50" />

<h5>Password</h5>
<?php echo form_error('user[user_password]'); ?>
<input type="text" name="user[user_password]" size="50" />

<h5>Password Confirm</h5>
<?php echo form_error('passconf'); ?>
<input type="text" name="passconf" size="50" />

<h5>First name</h5>
<?php echo form_error('user[user_first_name]'); ?>
<input type="text" name="user[user_first_name]" value="<?php echo set_value('user[user_first_name]',$user_first_name); ?>" size="50" />

<h5>Last name</h5>
<?php echo form_error('user[user_last_name]'); ?>
<input type="text" name="user[user_last_name]" value="<?php echo set_value('user[user_last_name]',$user_last_name); ?>" size="50" />

<h5>Speciality</h5>
<?php echo form_error('speciality[]'); ?>
<?php foreach($creative_fields as $cf) { ?>
<input type="checkbox" name="speciality[]" value="<?php echo $cf->creative_field_id;?>"
<?php echo (in_array($cf->creative_field_id, ($this->input->post('speciality') !== false) ? $this->input->post('speciality') : array())) ? "checked":"";?>>
	<?php echo $cf->creative_field_name_en."(".$cf->creative_field_name_id.")"?>
</input>
<?php } ?>


<div><input type="submit" value="Submit" /></div>

</form>

</body>
</html>
    <div class="main-content">
      <div class="container">
        <?php echo form_open("site/signup"); ?>
        <br>
        <h3>CREATE ACCOUNT</h3>
        <div class="form-container col-md-9">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <?php echo form_error('user[user_email]'); ?>
          <input type="email" class="form-control" id="exampleInputEmail1" name="user[user_email]" value="<?=@set_value('user[user_email]',$user_email)?>" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="Password1">Password</label>
          <?php echo form_error('user[user_password]'); ?>
          <input type="password" class="form-control" id="Password1" name="user[user_password]" placeholder="Password">
        </div>
        <div class="form-group">
          <label for="Password2">Confirm Password</label>
          <?php echo form_error('passconf'); ?>
          <input type="password" class="form-control" id="Password2" name="passconf" placeholder="Confirm password">
        </div>
        <div class="form-group">
          <label for="FirstName">First Name</label>
          <?php echo form_error('user[user_first_name]'); ?>
          <input type="text" class="form-control" id="FirstName" name="user[user_first_name]" value="<?=@set_value('user[user_first_name]',$user_first_name)?>"placeholder="First name">
        </div>
        <div class="form-group">
          <label for="LastName">Last Name</label>
          <?php echo form_error('user[user_last_name]'); ?>
          <input type="text" class="form-control" id="LastName" name="user[user_last_name]" value="<?=@set_value('user[user_last_name]',$user_last_name); ?>"placeholder="Last name">
        </div>
        <div class="form-group">
          <label class="tes2" for="checkbox">Choose Your Creative Fields</label>
          <?php echo form_error('speciality[]'); ?>
          <?php $size = count($creative_fields); $ii = 1; ?>
          <div class="col-sm-6">
          <?php foreach($creative_fields as $cf) { ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="speciality[]" value="<?=$cf->creative_field_id;?>" <?=(in_array($cf->creative_field_id, ($this->input->post('speciality') !== false) ? $this->input->post('speciality') : array())) ? "checked":""?>>
                <?=$cf->creative_field_name_en?>
              </label>
            </div>
            <?php if($ii == ceil($size/2)) {?>
              </div>
              <div class="col-sm-6">
            <?php } ?>
           <?php $ii++; ?>
          <?php } ?>
          </div>
        </div>
        <div class="form-group col-md-12"> 
        <input type="checkbox" required> I have read and accept the rules of this challenge</input> 
        <p><button href="<?=base_url()?>#" id="button-chalago-xl">CREATE <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button></p>
        </div>            
      </div>
      <?php echo form_close(); ?>
      <div class="col-md-3 other-ways">
        <?php $connect = $this->session->flashdata('connect');?>
        <?php if(!$connect) { ?> 
        <p>Or create your account with :</p>
        <a href="<?=base_url('auth/signup/fb')?>"><button id="button-chalago-FB">Facebook</button></a>
         <a href="<?=base_url('auth/signup/twitter0')?>"><button id="button-chalago-T">Twitter</button></a>
        <button id="button-chalago-G">Google+</button>
        
        <?php } else { ?>
          <?php $this->session->keep_flashdata('connect');?>
          <div>Connected with <?=$connect['provider']?></div>
          <div><img src="<?=$connect['avatar']?>"></div>
          <div><?=$connect['name']?></div>
          <div><a href=<?=base_url('auth/signup/remove')?>>Hapus integrasi</a></div>
        <?php } ?> 
      </div>
    </div> 
  </div>
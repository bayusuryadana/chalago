<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="<?=base_url()?>media/image/general/thumb.png" />
    <title>Chalago!</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>media/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>media/css/chalago.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/login.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?=base_url()?>../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
      <div class="logo">      
        <a href="<?=base_url()?>putu/index"><img src="<?=base_url()?>media/image/general/Chalago - Logo.png"></a>
      </div>
      <div class="login-container">
       <h1>SIGN IN TO YOUR CHALAGO! ACCOUNT<h1>
        <form action method="post">
          <input type="text" placeholder="Username" name="username" class="textbox" required>
          <input type="password" placeholder="Password" name="password" class="textbox" required>
          <div class="submit-container">
            <input type="checkbox" id="remember-me" value="1">
            <label for="remember-me">Remember me</label>
            <input type="submit" value="Login" name="login" id="button-chalago"></input>
          </div>
        </form>
        <hr>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>media/script/jquery.js"></script>
    <script src="<?=base_url()?>media/script/bootstrap.min.js"></script>
    <script src="<?=base_url()?>media/script/smooth-scroll.js"></script>
  
  </body>
</html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="<?=base_url()?>media/image/general/thumb.png" />
    <title>Chalago!</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>media/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>media/css/chalago.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/challenges.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/challenges-finished.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?=base_url()?>../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span><img src="<?=base_url()?>media/image/general/Home-Menu.png"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url()?>index.html"><img class="logo" src="<?=base_url()?>media/image/general/Chalago - Logo.png"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?=base_url()?>putu/view_more_challenges">DISCOVER</a></li>
            <li><a href="<?=base_url()?>#">LAUNCH</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=base_url()?>#">SIGN UP</a></li>
            <li><a href="<?=base_url()?>putu/login">LOGIN</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="header-challenge">

      <div class="menu-fix-when-scroll">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-12">
              <p>Illustration Challenge</p>
              <h1>TRANSFORMER AGE OF EXTINCTION<br>FAN ART CHALLENGE</h1>
              <p>Hasbro Agency</p>
            </div>
         </div>
        </div>
      </div>

    </div>


    <div class="timeline">
      ...
    </div>   

    <div class="main-content">
      <div class="container">
        <div class="row">
          <div class=" col-xs-12 col-md-9">
            <div class="content-container" id="summary">
            <h2>SUMMARY</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat.</p>
            </div>
            <div class="content-container" id="results">
            <h2>RESULT</h2>
              <div class="row container-result">
                <div class=" col-sm-5 col-md-5">                  
                  <img class="image" src="<?=base_url()?>media/image/challenges-finish/img01.png">
                </div>
                <div class="col-sm-7 col-md-7">
                  <h3>RUPA" WEWAYANGAN</h3>
                  <h4>Yisar Andrianus</h4>
                  <h6>Graphic Design & Illustration</h6>
                  <h6>Malang, Jawa Timur</h6>
                  <p id="medal"><span id="medal-icon"><img src="<?=base_url()?>media/image/challenges-finish/icon-medal1.png"></span><span id="prize-tag">1st Prize - </span>Rp. 25.000.000</p>
                  <p id="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>

              <div class="row container-result">
                <div class="col-md-5">                  
                  <img class="image" src="<?=base_url()?>media/image/challenges-finish/img02.png">
                </div>
                <div class="col-md-7">
                  <h3>RUPA" WEWAYANGAN</h3>
                  <h4>Yisar Andrianus</h4>
                  <h6>Graphic Design & Illustration</h6>
                  <h6>Malang, Jawa Timur</h6>
                  <p id="medal"><span id="medal-icon"><img src="<?=base_url()?>media/image/challenges-finish/icon-medal2.png"></span><span id="prize-tag">2nd Prize - </span>Rp. 15.000.000</p>
                  <p id="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>

              <div class="row container-result">
                <div class="col-md-5">                  
                  <img class="image" src="<?=base_url()?>media/image/challenges-finish/img03.png">
                </div>
                <div class="col-md-7">
                  <h3>RUPA" WEWAYANGAN</h3>
                  <h4>Yisar Andrianus</h4>
                  <h6>Graphic Design & Illustration</h6>
                  <h6>Malang, Jawa Timur</h6>
                  <p id="medal"><span id="medal-icon"><img src="<?=base_url()?>media/image/challenges-finish/icon-medal3.png"></span><span id="prize-tag">3rd Prize - </span>Rp. 5.000.000</p>
                  <p id="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>
            </div>
            <div class="content-container" id="submissions">
              <h2>ALL SUBMISSIONS (225)</h2>
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub01.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub02.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub03.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub04.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub05.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub06.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub06.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub05.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub04.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub03.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub02.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub01.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub01.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub02.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub03.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub04.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub05.png">
              <img src="<?=base_url()?>media/image/challenges-finish/img-sub06.png">
            </div>
            
            


          </div>
          <div class="col-xs-12 col-md-3">

            <div class="subcontent-prize" id="total-prize">
              <h3>TOTAL PRIZE</h3>
              <p>Rp. 50.000.000,00</p>
            </div>
            <div class="subcontent-deadline" id="deadline">
              <h3>DEADLINE</h3>
              <p>16 JULY 2014</p>
              <p id="time">23.59 WIB</p>
            </div>
            <div class="subcontent-jury">
              <h3>JURY</h3>
              <div class="row">
                <div class="col-xs-4"><img class="image" src="<?=base_url()?>media/image/challenges/img-jury01.png"></div>
                <div class="col-xs-8">
                  <p class="jury-title">JURY NAME 01</p>
                  <p class="jury-desc">Art Director</p>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-4"><img class="image" src="<?=base_url()?>media/image/challenges/img-jury02.png"></div>
                <div class="col-xs-8">
                  <p class="jury-title">JURY NAME 02</p>
                  <p class="jury-desc">Art Director</p>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4"><img class="image" src="<?=base_url()?>media/image/challenges/img-jury03.png"></div>
                <div class="col-xs-8">
                  <p class="jury-title">CHALLAGO!</p>
                  <p class="jury-desc">Art Director</p>
                </div>
              </div>
              
            </div>

            <div class="subcontent-jury-992">
              <h3>JURY</h3>
              <div class="row">
              <div class="col-xs-4">
                <img class="image" src="<?=base_url()?>media/image/challenges/img-jury01.png">
                <p class="jury-title">JURY NAME 01</p>
                <p class="jury-desc">Description</p>
              </div>
              <div class="col-xs-4">
                <img class="image" src="<?=base_url()?>media/image/challenges/img-jury02.png">
                <p class="jury-title">JURY NAME 01</p>
                <p class="jury-desc">Description</p>
              </div>
              <div class="col-xs-4">
                <img class="image" src="<?=base_url()?>media/image/challenges/img-jury03.png">
                <p class="jury-title">JURY NAME 01</p>
                <p class="jury-desc">Description</p>
              </div>

              
              </div>
            </div>

          </div>
        </div>
      </div>
    </div> 
  
    <footer>
      ....
    </footer> 
  

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>media/script/jquery.js"></script>
    <script src="<?=base_url()?>media/script/chalago.js"></script>
    <script src="<?=base_url()?>media/script/bootstrap.min.js"></script>
    <script src="<?=base_url()?>media/script/smooth-scroll.js"></script>
  
  </body>
</html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="<?=base_url()?>media/image/general/thumb.png" />
    <title>Chalago!</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>media/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>media/css/chalago.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/challenges.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/signup.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?=base_url()?>../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span><img src="<?=base_url()?>media/image/general/Home-Menu.png"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url()?>index.html"><img class="logo" src="<?=base_url()?>media/image/general/Chalago - Logo.png"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?=base_url()?>putu/view_more_challenges">DISCOVER</a></li>
            <li><a href="<?=base_url()?>#">LAUNCH</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=base_url()?>#">SIGN UP</a></li>
            <li><a href="<?=base_url()?>putu/login">LOGIN</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="main-content">
      <div class="container">

      <form role="form">
        <br>
        <h3>CREATE ACCOUNT</h3>
        <div class="form-container col-md-9">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="Password1">Password</label>
          <input type="password" class="form-control" id="Password1" placeholder="Password">
        </div>
        <div class="form-group">
          <label for="Password2">Confirm Password</label>
          <input type="password" class="form-control" id="Password2" placeholder="Confirm password">
        </div>
        <div class="form-group">
          <label for="FirstName">FIrst Name</label>
          <input type="password" class="form-control" id="FirstName" placeholder="First name">
        </div>
        <div class="form-group">
          <label for="LastName">Last Name</label>
          <input type="password" class="form-control" id="LastName" placeholder="Last name">
        </div>
        <div class="form-group">
          <label class="tes2" for="checkbox">Choose Your Creative Fields</label>
          <div class="col-sm-6">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Graphic Design 
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Copywriting
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Photography
              </label>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Illustration
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Video / Animation
              </label>
            </div>
           </div>               
        </div>
        <div class="form-group col-md-12"> 
        <input type="checkbox" required> I have read and accept the rules of this challenge</input> 
        <p><button href="<?=base_url()?>#" id="button-chalago-xl">CREATE <span><img class="arrow" src="<?=base_url()?>media/image/icon/arrow.png"></span></button></p>
        </div>            
      </div>
      <div class="col-md-3 other-ways">
        <p>Or create your account with :</p>
        <button id="button-chalago-FB">Facebook</button>
        <button id="button-chalago-T">Twitter</button>
        <button id="button-chalago-G">Google+</button>
      </div>
      </form>

    </div> 
  </div>
  
    <footer>
      ....
    </footer> 
  

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   
    <script src="<?=base_url()?>media/script/jquery.js"></script>
    <script src="<?=base_url()?>media/script/chalago.js"></script>
    <script src="<?=base_url()?>media/script/bootstrap.min.js"></script>
    <script src="<?=base_url()?>media/script/smooth-scroll.js"></script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?=base_url()?>media/script/vendor/jquery.ui.widget.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?=base_url()?>http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?=base_url()?>http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?=base_url()?>media/script/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-validate.js"></script>  
  </body>
</html>

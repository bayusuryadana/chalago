<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="<?=base_url()?>media/image/general/thumb.png" />
    <title>Chalago!</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>media/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>media/css/chalago.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/challenges.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/upload.css" rel="stylesheet">


    <!-- Generic page styles -->
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?=base_url()?>media/css/jquery.fileupload.css">



    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?=base_url()?>../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="<?=base_url()?>https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span><img src="<?=base_url()?>media/image/general/Home-Menu.png"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url()?>index.html"><img class="logo" src="<?=base_url()?>media/image/general/Chalago - Logo.png"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?=base_url()?>putu/view_more_challenges">DISCOVER</a></li>
            <li><a href="<?=base_url()?>#">LAUNCH</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=base_url()?>#">SIGN UP</a></li>
            <li><a href="<?=base_url()?>putu/login">LOGIN</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="header-challenge">

      <div class="menu-fix-when-scroll">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-12">
              <p>Illustration Challenge</p>
              <h1>TRANSFORMER AGE OF EXTINCTION<br>FAN ART CHALLENGE</h1>
              <p>Hasbro Agency</p>
            </div>
         </div>
        </div>
      </div>

    </div>

    <div class="timeline">
      ...
    </div>  

    <div class="main-content">
      <div class="container">

        <form class="form-horizontal" role="form">
          <h3>SELECT YOUR ENTRY</h3>
          <div class="form-group">
            <div class="col-sm-9 dropzone" id="dropzone">
              <button class="fileinput-button" id="button-chalago-s">
                <span>Add your file</span>
                <input id="fileupload" type="file" name="files[]" multiple>
              </button>
              <p><img src="<?=base_url()?>media/image/upload/img-addfile.png"></p>
              <p>Or drag your file here</p>
            </div>
             <div class="col-sm-3 warning">
              <p>Please upload your entry with these file type : <b>JPG, JPEG and PNG</b></p>
            </div>
             <div class="col-sm-3 attention">
              <p>Need help? Contact us through the <b><a href="<?=base_url()?>#">Support</a></b>. Any doubt? Read the <b><a href="<?=base_url()?>#">Guidelines</a></b>.</p>
            </div>
         </div>
         <div class="form-group">
          <div class="col-sm-9">
            
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>
            <!-- The container for the uploaded files -->
            <div id="files" class="files"></div>
          </div>

         </div>

          <h3>DESCRIBE YOUR ENTRY</h3>
          <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-7">
              <input type="email" class="form-control" id="inputTitle" placeholder="">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-7">
              <textarea class="form-control" rows="6"></textarea>
            </div>
          </div>
         
          <h3>LEGAL INFORMATION</h3>
          <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">My media shows</label>
            <div class="col-sm-7">
              <select class="form-control">
                <option>Adult(s) and I have no consent (I couldn’t obtain it)</option>
                <option>Minor(s) and I have no consent (I couldn’t obtain it)</option>
              </select>
            </div>
            <div class="col-sm-3 warning">
              <p>Please fill out this field</p>
            </div>
          </div>
          <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">My media shows</label>
            <div class="col-sm-7">
              <select class="form-control">
                <option>Adult(s) and I have no consent (I couldn’t obtain it)</option>
                <option>Minor(s) and I have no consent (I couldn’t obtain it)</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">My media plays</label>
            <div class="col-sm-7">
              <select class="form-control">
                <option>Adult(s) and I have no consent (I couldn’t obtain it)</option>
                <option>Minor(s) and I have no consent (I couldn’t obtain it)</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">My media presents</label>
            <div class="col-sm-7">
              <select class="form-control">
                <option>Adult(s) and I have no consent (I couldn’t obtain it)</option>
                <option>Minor(s) and I have no consent (I couldn’t obtain it)</option>
              </select>
            </div>
          </div>
          <div class="submit-container">
            <input type="submit" value="UPLOAD" id="button-chalago">
          </div>
        </form>
        
      </div>
    </div> 
  
    <footer>
      ....
    </footer> 
  

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   
    <script src="<?=base_url()?>media/script/jquery.js"></script>
    <script src="<?=base_url()?>media/script/chalago.js"></script>
    <script src="<?=base_url()?>media/script/bootstrap.min.js"></script>
    <script src="<?=base_url()?>media/script/smooth-scroll.js"></script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?=base_url()?>media/script/vendor/jquery.ui.widget.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?=base_url()?>http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?=base_url()?>http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?=base_url()?>media/script/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="<?=base_url()?>media/script/jquery.fileupload-validate.js"></script>
    <script>
      /*jslint unparam: true, regexp: true */
      /*global window, $ */
      $(function () {
          'use strict';
          // Change this to the location of your server-side upload handler:
          var url = window.location.hostname === 'blueimp.github.io' ?
                      '//jquery-file-upload.appspot.com/' : 'server/php/',
              uploadButton = $('<button/>')
                  .addClass('btn btn-primary')
                  .prop('disabled', true)
                  .text('Processing...')
                  .on('click', function () {
                      var $this = $(this),
                          data = $this.data();
                      $this
                          .off('click')
                          .text('Abort')
                          .on('click', function () {
                              $this.remove();
                              data.abort();
                          });
                      data.submit().always(function () {
                          $this.remove();
                      });
                  });
          $('#fileupload').fileupload({
              url: url,
              dataType: 'json',
              autoUpload: false,
              acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
              maxFileSize: 5000000, // 5 MB
              // Enable image resizing, except for Android and Opera,
              // which actually support image resizing, but fail to
              // send Blob objects via XHR requests:
              disableImageResize: /Android(?!.*Chrome)|Opera/
                  .test(window.navigator.userAgent),
              previewMaxWidth: 100,
              previewMaxHeight: 100,
              previewCrop: true
          }).on('fileuploadadd', function (e, data) {
              data.context = $('<div/>').appendTo('#files');
              $.each(data.files, function (index, file) {
                  var node = $('<p/>')
                          .append($('<span/>').text(file.name));
                  if (!index) {
                      node
                          .append('<br>')
                          .append(uploadButton.clone(true).data(data));
                  }
                  node.appendTo(data.context);
              });
          }).on('fileuploadprocessalways', function (e, data) {
              var index = data.index,
                  file = data.files[index],
                  node = $(data.context.children()[index]);
              if (file.preview) {
                  node
                      .prepend('<br>')
                      .prepend(file.preview);
              }
              if (file.error) {
                  node
                      .append('<br>')
                      .append($('<span class="text-danger"/>').text(file.error));
              }
              if (index + 1 === data.files.length) {
                  data.context.find('button')
                      .text('Upload')
                      .prop('disabled', !!data.files.error);
              }
          }).on('fileuploadprogressall', function (e, data) {
              var progress = parseInt(data.loaded / data.total * 100, 10);
              $('#progress .progress-bar').css(
                  'width',
                  progress + '%'
              );
          }).on('fileuploaddone', function (e, data) {
              $.each(data.result.files, function (index, file) {
                  if (file.url) {
                      var link = $('<a>')
                          .attr('target', '_blank')
                          .prop('href', file.url);
                      $(data.context.children()[index])
                          .wrap(link);
                  } else if (file.error) {
                      var error = $('<span class="text-danger"/>').text(file.error);
                      $(data.context.children()[index])
                          .append('<br>')
                          .append(error);
                  }
              });
          }).on('fileuploadfail', function (e, data) {
              $.each(data.files, function (index, file) {
                  var error = $('<span class="text-danger"/>').text('File upload failed.');
                  $(data.context.children()[index])
                      .append('<br>')
                      .append(error);
              });
          }).prop('disabled', !$.support.fileInput)
              .parent().addClass($.support.fileInput ? undefined : 'disabled');
      });
      </script>
  
  </body>
</html>

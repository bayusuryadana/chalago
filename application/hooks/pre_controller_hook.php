<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Hooks, do start() before loading a controller :3
 *
 */
class Pre_controller_hook{

	function __construct(){
	}

	public function start(){
		$maintenance = false;
		if($maintenance){
			$CI =& get_instance();
			if($CI->router->fetch_class() !== "site" || $CI->router->fetch_method() !== "maintenance"){
				redirect("site/maintenance");
			}
		}
	}
}
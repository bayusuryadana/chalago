<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct() 
	{ 
		parent::__construct(); 
	}
	
	public function brand()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = "Brand Contact Form";
        $data['css'] = array();
        $data['scripts'] = array();
        $data['page'] = 'site/brand';
		
        $this->form_validation->set_rules('brand_name', 'Nama Brand', 'required');
		$this->form_validation->set_rules('brand_cp_name', 'Nama CP', 'required');
		$this->form_validation->set_rules('brand_cp_no', 'Nomor CP', 'required');
		$this->form_validation->set_rules('brand_cp_email', 'Email ', 'required|valid_email');
		$this->form_validation->set_rules('description', 'Deskripsi', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			//form gagal
			$data['form_submit'] = false;
		}
		else
		{
			$data['form_submit'] = true;
			//lakuin kirim email
			//form berhasil
		}

       	$this->load->view('layout/main',$data);
	}
	public function support()
	{
		$data['title'] = "Support";
        $data['css'] = array();
        $data['scripts'] = array();
        $data['page'] = 'site/support';
       	$this->load->view('layout/main',$data);
	}

	public function about()
	{
		$data['title'] = "About";
        $data['css'] = array();
        $data['scripts'] = array();
        $data['page'] = 'site/about';
       	$this->load->view('layout/main',$data);
	}

	public function policy()
	{
		$data['title'] = "Privacy Policy";
        $data['css'] = array();
        $data['scripts'] = array();
        $data['page'] = 'site/policy';
       	$this->load->view('layout/main',$data);
	}

	public function terms()
	{
		$data['title'] = "Legal Terms";
        $data['css'] = array();
        $data['scripts'] = array();
        $data['page'] = 'site/terms';
       	$this->load->view('layout/main',$data);
	}	

	public function agreement()
	{
		$data['title'] = "Usage Agreement";
        $data['css'] = array();
        $data['scripts'] = array();
        $data['page'] = 'site/agreement';
       	$this->load->view('layout/main',$data);
	}

	public function maintenance(){
		echo "maintenacne";
	}
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	var $user_id;
	public function __construct() 
	{ 
		parent::__construct(); 
		$this->load->helper('form');
		$this->load->library('form_validation');
		if(!$this->authentication->is_logged_in())
		{
			redirect("home");
		}else{
			$this->user_id = $this->authentication->get_session()['user_id'];
		}
	}

	public function index()
	{
		echo json_encode($this->user_model->get_user(array('user_id'=>$this->user_id)));
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
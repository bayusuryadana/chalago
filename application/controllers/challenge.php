<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Challenge extends CI_Controller 
{
	public function __construct(){
        parent::__construct();
    }

    public function index()
    {
    	$this->load->model("challenge_model");
    	
    	$ongoings = $this->challenge_model->get_all_challenge("ongoing");
    	foreach ($ongoings as $row) {
    		$row->challenge_deadline = get_time_left($row->challenge_deadline);
    	}
    	$data["challenge_ongoing"] = $ongoings;

    	$finisheds = $this->challenge_model->get_all_challenge("finished");
    	foreach ($finisheds as $row) {
    		$row->challenge_deadline = get_time_left($row->challenge_deadline);
    	}
    	$data["challenge_finished"] = $finisheds;

		//setting header
		$data['title'] = "Chalago! ";
        $data['page'] = 'challenge-all';
        $data['meta_keywords'] = 'asdsad,adsadas,asdasdasd,adasdas,dsad';
        $data['mete_description'] = 'index';
        $data['scripts'] = array();
        $data['css'] = array("view-more-challenges.css");
        $this->load->view('layout/main',$data);
    }

    public function view($challenge_title = null, $challenge_id=null)
    {
    	$this->load->model("challenge_model");
    	$data['challenge'] = $this->challenge_model->get_challenge($challenge_id);

    	//setting header
        $data['title'] = "Chalago! ";
        $data['page'] = 'challenge-view';
        $data['meta_keywords'] = 'asdsad,adsadas,asdasdasd,adasdas,dsad';
        $data['mete_description'] = 'index';
        $data['scripts'] = array();
        $data['css'] = array("challenges.css");
        $this->load->view('layout/main',$data);
    }
}
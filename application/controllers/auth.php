<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() 
	{ 
		parent::__construct(); 
		$this->load->helper('form');
		$this->load->library('form_validation');

	}
	public function signup($provider = ""){
		$data['title'] = "Sigup to Chalago! ";
        $data['css'] = array('signup.css');
        $data['scripts'] = array('auth-google.js');
        $data['page'] = 'auth/signup';

		$this->form_validation->set_rules('user[user_email]', 'Email', 'required|valid_email|is_unique[us_ms_user.user_email]');
		$this->form_validation->set_message('is_unique', 'Email has already been registered');
		$this->form_validation->set_rules('user[user_password]', 'Password', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('user[user_first_name]', 'First Name', 'required');
		$this->form_validation->set_rules('speciality[]', 'Speciality', 'required');		
		
		if($provider =="remove"){
			$this->session->unset_userdata('connect');
			redirect("auth/signup");
		}else if($provider == "fb"){
			$this->load->library('fb/facebook');
			$user = $this->facebook->getUser();
			if ($user) {
	            try {
	                $data['user_profile'] = $this->facebook->api('/me');
	            } catch (FacebookApiException $e) {
	                $user = null;
	            }
	        }else {
	            $this->facebook->destroySession();
	        }
	        if ($user) {
	        	$data['login'] = $this->authentication->email_auth($data['user_profile']['email']);
            	if($data['login']['result']){
					redirect('auth/login');
                }else{
            		$connect['provider'] = "facebook";
            		$connect['avatar'] = "https://graph.facebook.com/".$data['user_profile']['id']."/picture?type=large";
                	$connect['name'] = $data['user_profile']['first_name']." ".$data['user_profile']['last_name'];
					$this->session->set_flashdata("connect",$connect);
				
                	$data['user_email'] = $data['user_profile']['email'];
                	$data['user_first_name'] = $data['user_profile']['first_name'];
                	$data['user_last_name'] = $data['user_profile']['last_name'];
            	}	
        	} else {
            	$login_url = $this->facebook->getLoginUrl(array(
                	'redirect_uri' => site_url('auth/signup/fb'), 
                	'scope' => array("email") // permissions here
           		));
            	redirect($login_url);
        	}		
		}else if($provider == "twitter0"){
			if($this->session->userdata("tw_access_token")){
				$this->session->sess_destroy();
			}
			$this->load->library('twitteroauth/twconnect');
			$ok = $this->twconnect->twredirect();
			if (!$ok) {
				echo 'Could not connect to Twitter. Refresh the page or try again later.';
			}
		}else if($provider == "twitter"){
			$this->load->library('twitteroauth/twconnect');
			$ok = $this->twconnect->twprocess_callback();
			if ( $ok ) { 
				$this->twconnect->twaccount_verify_credentials();
				$info = $this->twconnect->tw_user_info;
				//echo var_dump($info);
				$connect['provider'] = "twitter";
            	$connect['avatar'] = $info->profile_image_url;
                $connect['name'] = $info->name." (@".$info->screen_name.")";
				$this->session->set_userdata("connect",$connect);

				$data['user_email'] = "";
				$data['user_first_name'] = $info->name;
				$data['user_last_name'] = "";
			}
			else {
				redirect("auth/signup/twitter0");
				echo 'failed to login:'. $this->twconnect->tw_status;
			}		
		} else if($provider =="google0"){
			$post= $this->input->post();
			$connect['provider'] = "google";
           	$connect['avatar'] = $post['avatar'];
            $connect['name'] = $post['name'];
			$this->session->set_flashdata("connect",$connect);

			$google['email'] = $post['email'];
			$google['first_name'] = $post['first_name'];
			$google['last_name'] = $post['last_name'];
			$this->session->set_userdata("google",$google);
		}else if($provider == "google"){
			$google = $this->session->userdata("google");
			$data['login'] = $this->authentication->email_auth($google['email']);
            if($data['login']['result']){
				redirect('auth/login');
            }else {
          		$this->session->keep_flashdata("connect");  	
				$data['user_email'] = $google['email'];
				$data['user_first_name'] = $google['first_name'];
				$data['user_last_name'] = $google['last_name'];
				$this->session->unset_userdata("google");
			}
		}
		if ($this->form_validation->run() == FALSE)
		{
			$data['creative_fields'] = $this->system_model->get_creative_field();
			$this->load->view('layout/main',$data);
		}
		else
		{
			$this->session->unset_userdata("connect");
			$this->user_model->register_user($this->input->post('user'),$this->input->post('speciality'));
			$this->load->view('auth/register_success');
		}
	}
	public function verify($id_user = "",$activation_code ="")
	{
		if($id_user == "" || $activation_code == "")
		{
			echo "Missing argument";
		}else if(!$this->user_model->activate_user($id_user,$activation_code)){
			echo "aktivasi gagal, code aktivasi salah atau user sudah berhasil aktivasi";
		}else{
			echo "aktivasi berhasil :3";
		}
	}
	
	public function login()
	{
		if($this->authentication->is_logged_in()){
			redirect('dashboard');
		}
		$data['title'] = "Login to Chalago! ";
        $data['css'] = array('login.css');
        $data['page'] = 'auth/login';

		$this->form_validation->set_rules('user[user_email]', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('user[user_password]', 'Password', 'required');
		if ($this->form_validation->run() !== FALSE)
		{	
			$post = $this->input->post('user');
			$data['login'] = $this->authentication->login(1,$post['user_email'],$post['user_password']);
			
			if($data['login']['result']){
				echo "Berhasil login";
			}
		}
		$this->load->view('layout/one-page',$data);	
	}

	public function logout()
	{
		$this->authentication->logout();
		redirect("auth/login");
	}
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
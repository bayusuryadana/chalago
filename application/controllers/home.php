<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        //load active challenge
        $this->load->model("challenge_model");
        $challenges = $this->challenge_model->get_all_challenge("ongoing",4);
        foreach ($challenges as $row) {
            $row->challenge_deadline = get_time_left($row->challenge_deadline);
        }
    	$data['challenges'] = $challenges;

    	//load testimonials
    	$this->load->model("testimonial_model");
    	$data['testimonials'] = $this->testimonial_model->get_testimonial();

        //setting header
        $data['title'] = "Chalago! ";
        $data['page'] = 'home';
        $data['meta_keywords'] = 'asdsad,adsadas,asdasdasd,adasdas,dsad';
        $data['mete_description'] = 'index';
        $data['scripts'] = array();
        $data['css'] = array();
        $this->load->view('layout/main',$data);
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Putu extends CI_Controller {
	public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('putu/index');
    }
    public function view_challenges()
    {
        $this->load->view('putu/view-challenges');
    }
    public function upload(){
        $this->load->view('putu/upload');
    }
    public function view_challenges_finished(){
        $this->load->view('putu/view-challenges-finished');
    }
    public function view_more_challenges(){
        $this->load->view('putu/view-more-challenges');
    }
    public function login(){
        $this->load->view('putu/login');
    }
}

/* End of file tes.php */
/* Location: ./application/controllers/tes.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Authentication
{
	const SESSION_NAME = "session";

	const USER = 1;
	const ADMIN = 2;
	const JURY = 3;
	const BRAND = 4;

	public function __construct() 
	{ 
	
	}

	public function login($type, $identity, $password){
		$CI =& get_instance();
		$ret = "";
		if($type == self::USER){
			//$identity isinya email.
			$user = $CI->user_model->get_password(array('user_email'=>$identity));
			if($user === false){
				$ret['result'] = false;
				$ret['description'] = "email tidak ditemukan";
			}else if ($user->user_password !== $CI->user_model->get_hashed_password($password)){
				$ret['result'] = false;
				$ret['description'] = "password salah";
			}else{
				$ret['result'] = true;
				$ret['description'] = "login berhasil";
				$this->set_session(array('user_id'=>$user->user_id,'user_email'=>$user->user_email,'role'=>self::USER));
			}
		}else{
			$ret['result'] = false;
			$ret['description'] = "error";
		}
		return $ret;
	}
	
	public function email_auth($email)
	{
		$CI =& get_instance();
		$user = $CI->user_model->get_user(array('user_email'=>$email));
		if($user === false){
			$ret['result'] = false;
			$ret['description'] = "email tidak ditemukan";
		}else{
			$ret['result'] = true;
			$ret['description'] = "login berhasil";
			$this->set_session(array('user_id'=>$user->user_id,'user_email'=>$user->user_email,'role'=>self::USER));
		}
		return $ret;
	}
	public function logout(){
		//logout cookies web
		$CI =& get_instance();
		$CI->session->unset_userdata(self::SESSION_NAME);

		//logout facebook
		$CI->load->library('fb/facebook');
        $CI->facebook->destroySession();
	}

	public function set_session($data){
		$CI =& get_instance();
		$CI->session->set_userdata(self::SESSION_NAME, $data);
	}	

	public function get_session(){
		$CI =& get_instance();
		return $CI->session->userdata(self::SESSION_NAME);		
	}
	public function is_logged_in(){
		$CI =& get_instance();
		return ($this->get_session() == false ) ? false : true; 
	}
}
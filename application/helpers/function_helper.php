<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * fungsi yang mengembalikan nilai rentang waktu dari saat ini sampai dengan parameter yang diberikan
	 * @deadline type timestamp, waktu yang ingin diketahui rentangnya sampai dengan saat ini
	 */
    function get_time_left($deadline)
	{
        date_default_timezone_set('Asia/Jakarta');
		$now = date("Y-m-d H:i:s");
		$deadline_str = strtotime($deadline); //deadline in seconds
		$now_str = strtotime($now); //nowdays in seconds

		$diff = $deadline_str - $now_str;
	
		$hour   = 3600;
		$day    = 24*$hour;

		$ans["day"]    = floor(($diff)/$day);
		$ans["hour"]   = floor(($diff%$day)/$hour);

		if ($ans["day"] > 0){
			return $ans["day"] . " DAY(S) LEFT";
		} else 
		if ($ans["hour"] > 0){
			return $ans["hour"] . " HOUR(S) LEFT";
		} else {
			return "LESS THAN A HOUR LEFT";
		}
	}

	/**
	 * fungsi yang mengembalikan sebuah string yang mengganti character spasi dengan character dash
	 * @s type string, string yang ingin diubah
	 */ 
	function space_to_dash($s)
	{
		$slice = explode(" ", $s);
		$ans = "";
		for ($i=0; $i < count($slice); $i++) { 
			$ans .= $slice[$i];
			if ($i < count($slice)-1){
				$ans.="-";
			}
		}

		return $ans;
	}
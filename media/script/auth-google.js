 (function() {
   var po = document.createElement('script');
   po.type = 'text/javascript'; po.async = true;
   po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
   var s = document.getElementsByTagName('script')[0];
   s.parentNode.insertBefore(po, s);
 })();
function render() {
   // Additional params including the callback, the rest of the params will
   // come from the page-level configuration.
   var additionalParams = {
     'callback': signinCallback,
     'clientid': "558518193609-08u577erfjgkjhbqsjhc6b7tipuvns0c.apps.googleusercontent.com",
     'cookiepolicy' : "single_host_origin",
     'requestvisibleactions':"http://schema.org/AddAction",
     'scope':"https://www.googleapis.com/auth/plus.profile.emails.read",

   };

   // Attach a click listener to a button to trigger the flow.
   var signinButton = document.getElementById('button-chalago-G');
   signinButton.addEventListener('click', function() {
     gapi.auth.signIn(additionalParams); // Will use page level configuration
   });
 }

function signinCallback(authResult) {
    var email;
    if (authResult['status']['signed_in']) {
      gapi.auth.setToken(authResult);
      gapi.client.load('plus', 'v1', function(){
        var user = gapi.client.plus.people.get( {'userId' : 'me'} );   
        user.execute(function(profile){
          var email = profile['emails'].filter(function(v) {
              return v.type === 'account'; // Filter out the primary email
          })[0].value;
          var name = profile['displayName'];
          var first_name = profile['name']['givenName'];
          var last_name = profile['name']['familyName'];
          var avatar = profile['image']['url'];
          var token = authResult['access_token'];
          var form_data = {
            email : email,
            name : name,
            first_name : first_name,
            last_name : last_name,
            avatar : avatar,
            token:token
          }
          $.ajax({
                url: "http://localhost/chalago/site/signup/google0",
                type: "POST",
                data: form_data,
                beforeSend: function(){
                    //$("#signinButton").html("<img src='http://kapal.pandagostudio.com/application/assets/img/loading.gif'/>");
                },
                success: function(msg){
                  document.location = "http://localhost/chalago/site/signup/google";
                }
            });
        });
      });
    }else{
      console.log('Sign-in state: ' + authResult['error']);
    }
};
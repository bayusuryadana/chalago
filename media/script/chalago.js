$("document").ready(function($){
    var nav = $('.menu-fix-when-scroll');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 490) {
            nav.addClass("fixe");
        } else {
            nav.removeClass("fixe");
        }
    });
});
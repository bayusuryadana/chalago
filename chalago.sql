-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2014 at 09:17 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chalago`
--

-- --------------------------------------------------------

--
-- Table structure for table `ch_ms_brand`
--

CREATE TABLE IF NOT EXISTS `ch_ms_brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(100) NOT NULL,
  `brand_url` varchar(100) NOT NULL,
  `brand_username` varchar(20) NOT NULL,
  `brand_password` varchar(50) NOT NULL,
  `brand_description` varchar(500) NOT NULL,
  PRIMARY KEY (`brand_id`),
  UNIQUE KEY `brand_username` (`brand_username`),
  KEY `id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ch_ms_brand`
--

INSERT INTO `ch_ms_brand` (`brand_id`, `brand_name`, `brand_url`, `brand_username`, `brand_password`, `brand_description`) VALUES
(1, 'Nestle', 'www.nestle.com', 'nestle', 'b7597b8eb816aee066f98122ec00defe', 'Nestle itu keren');

-- --------------------------------------------------------

--
-- Table structure for table `ch_ms_challenge`
--

CREATE TABLE IF NOT EXISTS `ch_ms_challenge` (
  `challenge_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `challenge_title` varchar(100) NOT NULL,
  `challenge_brief` varchar(1000) NOT NULL,
  `challenge_rules` varchar(100) NOT NULL,
  `challenge_description` varchar(2000) NOT NULL,
  `total_prize_money` int(11) DEFAULT NULL,
  `total_prize_item` varchar(1000) DEFAULT NULL,
  `challenge_jury` varchar(300) NOT NULL,
  `challenge_deadline` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`challenge_id`),
  KEY `id_brand` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ch_ms_challenge`
--

INSERT INTO `ch_ms_challenge` (`challenge_id`, `brand_id`, `challenge_title`, `challenge_brief`, `challenge_rules`, `challenge_description`, `total_prize_money`, `total_prize_item`, `challenge_jury`, `challenge_deadline`) VALUES
(1, 1, 'Minum susu pagi hari!', 'Buat cara minum susu yang keren :3', 'bebas', ':3', 10000, 'tidak ada', ':3', '2014-06-28 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ch_tr_allowedfile`
--

CREATE TABLE IF NOT EXISTS `ch_tr_allowedfile` (
  `allowedfile_id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge_id` int(11) NOT NULL,
  `allowedfile_filetype` int(11) NOT NULL,
  `allowedfile_helptext` varchar(250) NOT NULL,
  PRIMARY KEY (`allowedfile_id`),
  KEY `id_challenge` (`challenge_id`),
  KEY `id_filetype` (`allowedfile_filetype`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ch_tr_allowedfile`
--

INSERT INTO `ch_tr_allowedfile` (`allowedfile_id`, `challenge_id`, `allowedfile_filetype`, `allowedfile_helptext`) VALUES
(1, 1, 0, 'berikan foto terbaikmu!');

-- --------------------------------------------------------

--
-- Table structure for table `ch_tr_category`
--

CREATE TABLE IF NOT EXISTS `ch_tr_category` (
  `challenge_id` int(11) NOT NULL,
  `creative_field_id` int(11) NOT NULL,
  PRIMARY KEY (`challenge_id`,`creative_field_id`),
  KEY `id_cr_field` (`creative_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ch_tr_category`
--

INSERT INTO `ch_tr_category` (`challenge_id`, `creative_field_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ch_tr_prize`
--

CREATE TABLE IF NOT EXISTS `ch_tr_prize` (
  `prize_id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge_id` int(11) NOT NULL,
  `prize_rank` int(11) NOT NULL,
  `prize_money` int(11) DEFAULT NULL,
  `prize_item` varchar(500) NOT NULL,
  `submission_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prize_id`),
  KEY `id_challenge` (`challenge_id`),
  KEY `id_winner` (`submission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ch_tr_prize`
--

INSERT INTO `ch_tr_prize` (`prize_id`, `challenge_id`, `prize_rank`, `prize_money`, `prize_item`, `submission_id`) VALUES
(1, 1, 1, 5000000, 'jalan-jalan bersama bayu :3', NULL),
(2, 1, 2, 500000, 'tidur bersama bayu :3', NULL),
(3, 1, 3, 2000, 'cium bayu :3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `su_ms_jury`
--

CREATE TABLE IF NOT EXISTS `su_ms_jury` (
  `jury_id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge_id` int(11) NOT NULL,
  `jury_username` varchar(20) NOT NULL,
  `jury_password` varchar(50) NOT NULL,
  `jury_name` varchar(100) NOT NULL,
  `jury_description` varchar(500) NOT NULL,
  PRIMARY KEY (`jury_id`),
  KEY `id_challenge` (`challenge_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `su_ms_jury`
--

INSERT INTO `su_ms_jury` (`jury_id`, `challenge_id`, `jury_username`, `jury_password`, `jury_name`, `jury_description`) VALUES
(1, 1, 'juri1', 'b7597b8eb816aee066f98122ec00defe', 'Juri 1', 'CEO Kambing Conge');

-- --------------------------------------------------------

--
-- Table structure for table `su_ms_submission`
--

CREATE TABLE IF NOT EXISTS `su_ms_submission` (
  `submission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `submission_title` varchar(200) NOT NULL,
  `submission_description` varchar(10000) NOT NULL,
  `submission_status` int(1) NOT NULL,
  `submission_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`submission_id`),
  KEY `id_user` (`user_id`),
  KEY `id_challenge` (`challenge_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `su_ms_submission`
--

INSERT INTO `su_ms_submission` (`submission_id`, `user_id`, `challenge_id`, `submission_title`, `submission_description`, `submission_status`, `submission_timestamp`) VALUES
(1, 2, 1, 'Minum Sehat', 'Foto ini menunjukkan betapa bisa menyehatkannya sebuah minuman', 0, '2014-06-18 10:16:00');

-- --------------------------------------------------------

--
-- Table structure for table `su_tr_feedback`
--

CREATE TABLE IF NOT EXISTS `su_tr_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `submission_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `content` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_submission` (`submission_id`),
  KEY `id_brand` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `su_tr_file`
--

CREATE TABLE IF NOT EXISTS `su_tr_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) NOT NULL,
  `allowedfile_id` int(11) NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `id_submission` (`submission_id`),
  KEY `id_allowed_file` (`allowedfile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `su_tr_score`
--

CREATE TABLE IF NOT EXISTS `su_tr_score` (
  `submission_id` int(11) NOT NULL,
  `jury_id` int(11) NOT NULL,
  `score_value` int(3) NOT NULL,
  `score_notes` varchar(500) NOT NULL,
  PRIMARY KEY (`submission_id`,`jury_id`),
  KEY `id_jury` (`jury_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_ms_admin`
--

CREATE TABLE IF NOT EXISTS `sys_ms_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(20) NOT NULL,
  `admin_password` varchar(50) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_ms_admin`
--

INSERT INTO `sys_ms_admin` (`admin_id`, `admin_username`, `admin_password`) VALUES
(1, 'admin', 'b7597b8eb816aee066f98122ec00defe');

-- --------------------------------------------------------

--
-- Table structure for table `sys_ms_badge`
--

CREATE TABLE IF NOT EXISTS `sys_ms_badge` (
  `badge_id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_name` varchar(100) NOT NULL,
  `badge_order` int(3) NOT NULL,
  PRIMARY KEY (`badge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_ms_city`
--

CREATE TABLE IF NOT EXISTS `sys_ms_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `id_province` (`province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sys_ms_city`
--

INSERT INTO `sys_ms_city` (`city_id`, `province_id`, `city_name`) VALUES
(1, 1, 'Bogor'),
(2, 1, 'Depok'),
(3, 2, 'Kansas'),
(4, 2, 'Kanada');

-- --------------------------------------------------------

--
-- Table structure for table `sys_ms_creative_field`
--

CREATE TABLE IF NOT EXISTS `sys_ms_creative_field` (
  `creative_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `creative_field_name_en` varchar(100) NOT NULL,
  `creative_field_name_id` varchar(100) NOT NULL,
  PRIMARY KEY (`creative_field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sys_ms_creative_field`
--

INSERT INTO `sys_ms_creative_field` (`creative_field_id`, `creative_field_name_en`, `creative_field_name_id`) VALUES
(1, 'Graphic Desain', 'Desain Grafis'),
(2, 'Video/Animation', 'Film/Animasi'),
(3, 'Copywriting', 'Penulisan Naskah'),
(4, 'Illustration', 'Ilustrasi'),
(5, 'Photography', 'Fotografi');

-- --------------------------------------------------------

--
-- Table structure for table `sys_ms_province`
--

CREATE TABLE IF NOT EXISTS `sys_ms_province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_name` varchar(50) NOT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sys_ms_province`
--

INSERT INTO `sys_ms_province` (`province_id`, `province_name`) VALUES
(1, 'Jawa Barat'),
(2, 'Jawa Timur');

-- --------------------------------------------------------

--
-- Table structure for table `sys_ms_testimonial`
--

CREATE TABLE IF NOT EXISTS `sys_ms_testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `testimonial_name` varchar(30) NOT NULL,
  `testimonial_profession` varchar(30) NOT NULL,
  `testimonial_content` varchar(256) NOT NULL,
  `testimonial_status` int(1) NOT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_ms_testimonial`
--

INSERT INTO `sys_ms_testimonial` (`testimonial_id`, `testimonial_name`, `testimonial_profession`, `testimonial_content`, `testimonial_status`) VALUES
(1, 'Bayu Suryadana', 'Suami Indar Fauziah', 'Website ini bagus, membuat saya dapat menyalurkan hobi desain saya :D', 1);

-- --------------------------------------------------------

--
-- Table structure for table `us_ms_notification`
--

CREATE TABLE IF NOT EXISTS `us_ms_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `notification_read_stat` int(1) NOT NULL,
  `notification_category` int(1) NOT NULL,
  `notification_link` int(1) NOT NULL,
  `link_to_page` varchar(100) NOT NULL,
  `notification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`notification_id`),
  KEY `id_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `us_ms_user`
--

CREATE TABLE IF NOT EXISTS `us_ms_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(254) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_first_name` varchar(50) NOT NULL,
  `user_last_name` varchar(50) DEFAULT NULL,
  `user_description` varchar(250) DEFAULT NULL,
  `user_kreavi_account` varchar(20) DEFAULT NULL,
  `user_gender` tinyint(1) DEFAULT NULL,
  `user_birthday` timestamp NULL DEFAULT NULL,
  `user_school` varchar(50) DEFAULT NULL,
  `user_company_profession` varchar(100) DEFAULT NULL,
  `user_company_name` varchar(100) DEFAULT NULL,
  `user_address` varchar(250) DEFAULT NULL,
  `user_location` int(11) DEFAULT NULL,
  `user_phone` int(14) DEFAULT NULL,
  `user_payment_bank` int(32) DEFAULT NULL,
  `user_payment_bank_branch` int(100) DEFAULT NULL,
  `user_payment_bank_number` int(20) DEFAULT NULL,
  `user_join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`user_email`),
  KEY `id` (`user_id`),
  KEY `user_location` (`user_location`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `us_ms_user`
--

INSERT INTO `us_ms_user` (`user_id`, `user_email`, `user_password`, `user_first_name`, `user_last_name`, `user_description`, `user_kreavi_account`, `user_gender`, `user_birthday`, `user_school`, `user_company_profession`, `user_company_name`, `user_address`, `user_location`, `user_phone`, `user_payment_bank`, `user_payment_bank_branch`, `user_payment_bank_number`, `user_join_date`, `user_last_login`) VALUES
(2, 'carakanurazmi@gmail.com', 'b7597b8eb816aee066f98122ec00defe', 'caraka', 'nur azmi', 'saya adalah anak gembala, selalu riang bersama teman. lalalalalalala', NULL, 0, '2014-06-17 17:00:00', 'UI', 'Coder', 'PandagoStudio', 'mau tau aja', 1, NULL, NULL, NULL, NULL, '2014-06-18 09:57:02', '0000-00-00 00:00:00'),
(6, 'caraka@gmail.com', '0d5259b97a5b02481bd77368626ea857', 'caraka', 'nur azmi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-24 06:20:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `us_tr_badge_owner`
--

CREATE TABLE IF NOT EXISTS `us_tr_badge_owner` (
  `badge_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `badge_owner_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`badge_id`,`user_id`),
  UNIQUE KEY `id_badge_3` (`badge_id`,`user_id`),
  KEY `id_badge` (`badge_id`),
  KEY `id_user` (`user_id`),
  KEY `id_badge_2` (`badge_id`),
  KEY `id_user_2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `us_tr_join`
--

CREATE TABLE IF NOT EXISTS `us_tr_join` (
  `user_id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `join_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`challenge_id`),
  KEY `id_challenge` (`challenge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `us_tr_join`
--

INSERT INTO `us_tr_join` (`user_id`, `challenge_id`, `join_timestamp`) VALUES
(2, 1, '2014-06-18 10:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `us_tr_speciality`
--

CREATE TABLE IF NOT EXISTS `us_tr_speciality` (
  `user_id` int(11) NOT NULL,
  `creative_field_id` int(11) NOT NULL,
  `speciality_order` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`creative_field_id`),
  KEY `id_cr_field` (`creative_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `us_tr_speciality`
--

INSERT INTO `us_tr_speciality` (`user_id`, `creative_field_id`, `speciality_order`) VALUES
(2, 1, 0),
(6, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `us_tr_special_stat`
--

CREATE TABLE IF NOT EXISTS `us_tr_special_stat` (
  `user_id` int(11) NOT NULL,
  `special_stat_code` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `us_tr_special_stat`
--

INSERT INTO `us_tr_special_stat` (`user_id`, `special_stat_code`) VALUES
(6, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ch_ms_challenge`
--
ALTER TABLE `ch_ms_challenge`
  ADD CONSTRAINT `ch_ms_challenge_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `ch_ms_brand` (`brand_id`) ON UPDATE CASCADE;

--
-- Constraints for table `ch_tr_allowedfile`
--
ALTER TABLE `ch_tr_allowedfile`
  ADD CONSTRAINT `ch_tr_allowedfile_ibfk_1` FOREIGN KEY (`challenge_id`) REFERENCES `ch_ms_challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ch_tr_category`
--
ALTER TABLE `ch_tr_category`
  ADD CONSTRAINT `ch_tr_category_ibfk_1` FOREIGN KEY (`challenge_id`) REFERENCES `ch_ms_challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ch_tr_category_ibfk_2` FOREIGN KEY (`creative_field_id`) REFERENCES `sys_ms_creative_field` (`creative_field_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ch_tr_prize`
--
ALTER TABLE `ch_tr_prize`
  ADD CONSTRAINT `ch_tr_prize_ibfk_3` FOREIGN KEY (`challenge_id`) REFERENCES `ch_ms_challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ch_tr_prize_ibfk_4` FOREIGN KEY (`submission_id`) REFERENCES `su_ms_submission` (`submission_id`) ON UPDATE CASCADE;

--
-- Constraints for table `su_ms_jury`
--
ALTER TABLE `su_ms_jury`
  ADD CONSTRAINT `su_ms_jury_ibfk_1` FOREIGN KEY (`challenge_id`) REFERENCES `ch_ms_challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `su_ms_submission`
--
ALTER TABLE `su_ms_submission`
  ADD CONSTRAINT `su_ms_submission_ibfk_2` FOREIGN KEY (`challenge_id`) REFERENCES `ch_ms_challenge` (`challenge_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `su_ms_submission_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `us_ms_user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `su_tr_feedback`
--
ALTER TABLE `su_tr_feedback`
  ADD CONSTRAINT `su_tr_feedback_ibfk_1` FOREIGN KEY (`submission_id`) REFERENCES `su_ms_submission` (`submission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `su_tr_feedback_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `ch_ms_brand` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `su_tr_file`
--
ALTER TABLE `su_tr_file`
  ADD CONSTRAINT `su_tr_file_ibfk_2` FOREIGN KEY (`allowedfile_id`) REFERENCES `ch_tr_allowedfile` (`allowedfile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `su_tr_file_ibfk_3` FOREIGN KEY (`submission_id`) REFERENCES `su_ms_submission` (`submission_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `su_tr_score`
--
ALTER TABLE `su_tr_score`
  ADD CONSTRAINT `su_tr_score_ibfk_1` FOREIGN KEY (`submission_id`) REFERENCES `su_ms_submission` (`submission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `su_tr_score_ibfk_2` FOREIGN KEY (`jury_id`) REFERENCES `su_ms_jury` (`jury_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sys_ms_city`
--
ALTER TABLE `sys_ms_city`
  ADD CONSTRAINT `sys_ms_city_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `sys_ms_province` (`province_id`) ON UPDATE CASCADE;

--
-- Constraints for table `us_ms_notification`
--
ALTER TABLE `us_ms_notification`
  ADD CONSTRAINT `us_ms_notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `us_ms_user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `us_ms_user`
--
ALTER TABLE `us_ms_user`
  ADD CONSTRAINT `us_ms_user_ibfk_1` FOREIGN KEY (`user_location`) REFERENCES `sys_ms_city` (`city_id`) ON UPDATE CASCADE;

--
-- Constraints for table `us_tr_badge_owner`
--
ALTER TABLE `us_tr_badge_owner`
  ADD CONSTRAINT `us_tr_badge_owner_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `us_ms_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `us_tr_badge_owner_ibfk_3` FOREIGN KEY (`badge_id`) REFERENCES `sys_ms_badge` (`badge_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `us_tr_join`
--
ALTER TABLE `us_tr_join`
  ADD CONSTRAINT `us_tr_join_ibfk_2` FOREIGN KEY (`challenge_id`) REFERENCES `ch_ms_challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `us_tr_join_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `us_ms_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `us_tr_speciality`
--
ALTER TABLE `us_tr_speciality`
  ADD CONSTRAINT `us_tr_speciality_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `us_ms_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `us_tr_speciality_ibfk_2` FOREIGN KEY (`creative_field_id`) REFERENCES `sys_ms_creative_field` (`creative_field_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `us_tr_special_stat`
--
ALTER TABLE `us_tr_special_stat`
  ADD CONSTRAINT `us_tr_special_stat_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `us_ms_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
